import React from "react";
import ArticleList from "./ArticleList";

import "./App.scss";

//Construct url for search query
const apiUrl = (value, page, hitsPerPage) =>
  `https://hn.algolia.com/api/v1/search?query=${value}&page=${page}&hitsPerPage=${hitsPerPage}`;

//Construct url for front page
const frontPageUrl = (page, hitsPerPage) =>
  `http://hn.algolia.com/api/v1/search?tags=front_page&page=${page}&hitsPerPage=${hitsPerPage}`;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hits: [],
      page: 0,
      hitsPerPage: 10,
      totalPages: null,
      frontPage: true,
      isLoading: false
    };

    this.input = null;
  }

  componentDidMount() {
    this.fetchFrontPage();
  }

  //Search function returns frontpage news if value is empty otherwise returns search result
  handleSearch = e => {
    e.preventDefault();
    const { value } = this.input;
    if (value === "") {
      this.setState({ frontPage: true }, () => this.fetchFrontPage());
    } else {
      this.fetchArticles(value, 0);
      this.setState({ frontPage: false });
    }
  };

  //Previous button handler
  handlePagePrevious = e =>
    this.state.frontPage
      ? this.fetchFrontPage(this.state.page - 1)
      : this.fetchArticles(this.input.value, this.state.page - 1);

  //Next button handler
  handlePageNext = e =>
    this.state.frontPage
      ? this.fetchFrontPage(this.state.page + 1)
      : this.fetchArticles(this.input.value, this.state.page + 1);

  //Base fetch function
  fetchBase = url => {
    this.setState({ isLoading: true });
    return fetch(url).then(response => response.json());
  };

  //Fetch articles
  fetchArticles = (value, page, hitsPerPage = this.state.hitsPerPage) => {
    const url = apiUrl(value, page, hitsPerPage);
    this.fetchBase(url)
      .then(result => this.handleResult(result, page))
      .catch(error => {
        console.error(error);
      });
  };

  //Fetch frontpage
  fetchFrontPage = (page = 0, hitsPerPage = this.state.hitsPerPage) => {
    const url = frontPageUrl(page, hitsPerPage);
    this.fetchBase(url)
      .then(result => this.handleResult(result, page))
      .catch(error => {
        console.error(error);
      });
  };

  //Handle response
  handleResult = result =>
    this.setState({
      isLoading: false,
      hits: result.hits,
      page: result.page,
      totalPages: Math.ceil(result.nbHits / result.hitsPerPage)
    });

  render() {
    return (
      <div className="container">
        <div className="form-container">
          <form
            type="submit"
            onSubmit={this.handleSearch}
            className="search-form"
          >
            <input
              type="text"
              className="search-input"
              ref={node => (this.input = node)}
            />
            <button type="submit" className="search-button">
              <svg className="submit-button" viewBox="0 0 32 32">
                <path d="M 19.5 3 C 14.26514 3 10 7.2651394 10 12.5 C 10 14.749977 10.810825 16.807458 12.125 18.4375 L 3.28125 27.28125 L 4.71875 28.71875 L 13.5625 19.875 C 15.192542 21.189175 17.250023 22 19.5 22 C 24.73486 22 29 17.73486 29 12.5 C 29 7.2651394 24.73486 3 19.5 3 z M 19.5 5 C 23.65398 5 27 8.3460198 27 12.5 C 27 16.65398 23.65398 20 19.5 20 C 15.34602 20 12 16.65398 12 12.5 C 12 8.3460198 15.34602 5 19.5 5 z" />
              </svg>
            </button>
          </form>
        </div>

        {this.state.isLoading ? (
          <div class="loading-indicator"> ...loading</div>
        ) : (
          <ArticleList
            list={this.state.hits}
            page={this.state.page}
            totalPages={this.state.totalPages}
            handlePageNext={this.handlePageNext}
            handlePagePrevious={this.handlePagePrevious}
          />
        )}
      </div>
    );
  }
}

export default App;
