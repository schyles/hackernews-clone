import React from "react";

const ArticleList = ({
  list,
  page,
  totalPages,
  handlePageNext,
  handlePagePrevious
}) => (
  <div>
    <div className="article-list">
      {list.map(item => (
        <div className="article" key={item.objectID}>
          <a href={item.url}>{item.title}</a>
        </div>
      ))}
    </div>

    <div className="button-container">
      {page !== null && (
        <div className="button-group">
          <button
            type="button"
            className="btn"
            onClick={handlePagePrevious}
            disabled={page === 0 ? true : false}
          >
            Previous
          </button>
          <div className="page-info">
            Page {page + 1} of {totalPages}
          </div>
          <button type="button" className="btn" onClick={handlePageNext}>
            Next
          </button>
        </div>
      )}
    </div>
  </div>
);

export default ArticleList;
